package com.example.miaosha.mapper;

import com.example.miaosha.dataobject.SequenceDO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SequenceMapper {
    SequenceDO getByName(String name);

    void updateSelective(SequenceDO sequenceDO);
}
