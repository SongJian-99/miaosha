package com.example.miaosha.mapper;

import com.example.miaosha.dataobject.UserDO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserMapper {
    UserDO findUserById(Integer id);

    void insertSelective(UserDO userDO);

    UserDO findByTelephone(String telephone);
}
