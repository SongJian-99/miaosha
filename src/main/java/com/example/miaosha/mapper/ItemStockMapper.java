package com.example.miaosha.mapper;

import com.example.miaosha.dataobject.ItemStockDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ItemStockMapper {

    void insertSelective(ItemStockDO itemStockDO);

    ItemStockDO getByItemId(Integer id);

    int decreaseStock(@Param("itemId") Integer itemId, @Param("amount") Integer amount);
}
