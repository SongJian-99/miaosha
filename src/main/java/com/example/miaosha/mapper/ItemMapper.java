package com.example.miaosha.mapper;

import com.example.miaosha.controller.viewobject.ItemVO;
import com.example.miaosha.dataobject.ItemDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ItemMapper {


    void insertSelective(ItemDO itemDO);

    ItemDO getItemById(Integer id);

    List<ItemDO> listItem();

    void increaseSales(@Param("id") Integer id,@Param("amount") Integer amount);
}
