package com.example.miaosha.mapper;

import com.example.miaosha.dataobject.PromoDO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface PromoMapper {
    PromoDO getPromoByItemId(Integer itemId);

    PromoDO getPromoById(Integer promoId);
}
