package com.example.miaosha.mapper;

import com.example.miaosha.dataobject.StockLogDO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface StockLogMapper {
    void insertSelective(StockLogDO stockLogDO);

    StockLogDO selectById(String stockLogId);

    void updateByIdSelective(StockLogDO stockLogDO);
}
