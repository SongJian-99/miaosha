package com.example.miaosha.mapper;

import com.example.miaosha.dataobject.UserPasswordDO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserPasswordMapper {
    UserPasswordDO findByUserId(Integer id);

    void insertSelective(UserPasswordDO userPasswordDO);
}
