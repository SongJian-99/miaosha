package com.example.miaosha.mapper;

import com.example.miaosha.dataobject.OrderDO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface OrderMapper {
    void insertSelective(OrderDO orderDO);
}
