package com.example.miaosha.service;

import com.example.miaosha.error.BusinessException;
import com.example.miaosha.service.model.ItemModel;

import java.util.List;

public interface ItemService {
    /**
     * 创建商品
     * @param itemModel
     * @return
     */
    ItemModel createItem(ItemModel itemModel) throws BusinessException;

    /**
     * 商品的列表展示
     * @return
     */
    List<ItemModel> listItem();

    /**
     * 商品详情浏览
     * @param id 根据id查询商品信息
     * @return
     */
    ItemModel getItemById(Integer id);

    /**
     * 库存扣减
     * @param itemId
     * @param amount
     * @return
     */
    boolean decreaseStock(Integer itemId,Integer amount);

    //库存回补
    boolean increaseStock(Integer itemId,Integer amount);

    //异步更新库存
    boolean asyncDecreaseStock(Integer itemId,Integer amount);

    //初始化库存流水
    String initStockLog(Integer itemId,Integer amount);

    /**
     * 下单增加销量
     * @param id
     * @param amount
     */
    void increaseSales(Integer id,Integer amount);

    /**
     * item及promo缓存模型
     * @param id
     * @return
     */
    ItemModel getItemByIdInCache(Integer id);
}
