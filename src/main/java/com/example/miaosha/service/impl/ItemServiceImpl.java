package com.example.miaosha.service.impl;

import com.example.miaosha.controller.validator.ValidationResult;
import com.example.miaosha.controller.validator.ValidatorImpl;
import com.example.miaosha.controller.viewobject.ItemVO;
import com.example.miaosha.dataobject.ItemDO;
import com.example.miaosha.dataobject.ItemStockDO;
import com.example.miaosha.dataobject.StockLogDO;
import com.example.miaosha.error.BusinessException;
import com.example.miaosha.error.EmBusinessError;
import com.example.miaosha.mapper.ItemMapper;
import com.example.miaosha.mapper.ItemStockMapper;
import com.example.miaosha.mapper.StockLogMapper;
import com.example.miaosha.mq.MqProducer;
import com.example.miaosha.service.ItemService;
import com.example.miaosha.service.model.ItemModel;
import com.example.miaosha.service.model.PromoModel;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class ItemServiceImpl implements ItemService {
    @Resource
    private ValidatorImpl validator;

    @Resource
    private ItemMapper itemMapper;

    @Resource
    private ItemStockMapper itemStockMapper;

    @Resource
    private PromoServiceImpl promoService;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private MqProducer producer;

    @Resource
    private StockLogMapper stockLogMapper;

    @Override
    @Transactional
    public ItemModel createItem(ItemModel itemModel) throws BusinessException {
        //参数校验
        ValidationResult result = validator.validate(itemModel);
        if (result.isHasErrors()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, result.getErrMsg());
        }
        //转化ItemModel -> dataobject
        ItemDO itemDO = convertItemDOFromItemModel(itemModel);
        //写入数据库
        itemMapper.insertSelective(itemDO);
        itemModel.setId(itemDO.getId());
        ItemStockDO itemStockDO = convertItemStackDOFromItemModel(itemModel);
        itemStockMapper.insertSelective(itemStockDO);
        //返回创建完成的对象
        return getItemById(itemModel.getId());
    }

    private ItemDO convertItemDOFromItemModel(ItemModel itemModel) {
        if (itemModel == null) {
            return null;
        }
        ItemDO itemDO = new ItemDO();
        BeanUtils.copyProperties(itemModel, itemDO);
        itemDO.setPrice(itemModel.getPrice().doubleValue());
        return itemDO;
    }

    private ItemStockDO convertItemStackDOFromItemModel(ItemModel itemModel) {
        if (itemModel == null) {
            return null;
        }
        ItemStockDO itemStockDO = new ItemStockDO();
        itemStockDO.setItemId(itemModel.getId());
        itemStockDO.setStock(itemModel.getStock());
        return itemStockDO;
    }

    @Override
    public List<ItemModel> listItem() {
        List<ItemDO> itemDOList = itemMapper.listItem();
        List<ItemModel> itemModelList = new ArrayList<>();
        for (ItemDO itemDO : itemDOList) {
            ItemStockDO itemStockDO = itemStockMapper.getByItemId(itemDO.getId());
            itemModelList.add(convertModelFromDataObject(itemDO, itemStockDO));
        }
        return itemModelList;
    }

    @Override
    public ItemModel getItemById(Integer id) {
        ItemDO itemDO = itemMapper.getItemById(id);
        if (itemDO == null) {
            return null;
        }
        //操作获得库存数量
        ItemStockDO itemStockDO = itemStockMapper.getByItemId(itemDO.getId());

        ItemModel itemModel = convertModelFromDataObject(itemDO, itemStockDO);
        //获得活动商品信息
        PromoModel promoModel = promoService.getPromoByItemId(itemModel.getId());
        if(promoModel != null && promoModel.getStatus().intValue() != 3){
            itemModel.setPromoModel(promoModel);
        }
        return itemModel;
    }

    @Override
    @Transactional
    public boolean decreaseStock(Integer itemId, Integer amount) {
//        int affectedRow = itemStockMapper.decreaseStock(itemId, amount);
        long result = redisTemplate.opsForValue().increment("promo_item_stock_" + itemId,amount.intValue() * -1);
        if (result > 0) {
            //库存更新成功
            return true;
        }else if(result == 0){
            redisTemplate.opsForValue().set("promo_item_stock_invalid","true");
            return true;
        } else {
            //库存更新失败
//            redisTemplate.opsForValue().increment("promo_item_stock_" + itemId,amount.intValue());
            increaseStock(itemId,amount);
            return false;
        }
    }

    @Override
    public boolean increaseStock(Integer itemId, Integer amount) {
        redisTemplate.opsForValue().increment("promo_item_stock_" + itemId,amount.intValue());
        return true;
    }

    @Override
    public boolean asyncDecreaseStock(Integer itemId, Integer amount) {
        boolean mqResult = producer.asyncReduceStock(itemId,amount);
        return mqResult;
    }

    //初始化对应的库存流水
    @Override
    @Transactional
    public String initStockLog(Integer itemId, Integer amount) {
        StockLogDO stockLogDO = new StockLogDO();
        stockLogDO.setItemId(itemId);
        stockLogDO.setAmount(amount);
        stockLogDO.setStockLogId(UUID.randomUUID().toString().replace("-",""));
        stockLogDO.setStatus(1);
        stockLogMapper.insertSelective(stockLogDO);
        return stockLogDO.getStockLogId();
    }

    @Override
    @Transactional
    public void increaseSales(Integer id, Integer amount) {
        itemMapper.increaseSales(id,amount);
    }

    @Override
    public ItemModel getItemByIdInCache(Integer id) {
        ItemModel itemModel = (ItemModel)redisTemplate.opsForValue().get("item_validate_" + id);
        if(itemModel == null){
            itemModel = this.getItemById(id);
            redisTemplate.opsForValue().set("item_validate_" + id,itemModel);
            redisTemplate.expire("item_validate_" + id,10, TimeUnit.MINUTES);
        }
        return itemModel;
    }

    private ItemModel convertModelFromDataObject(ItemDO itemDO, ItemStockDO itemStockDO) {
        ItemModel itemModel = new ItemModel();
        BeanUtils.copyProperties(itemDO, itemModel);
        itemModel.setPrice(new BigDecimal(itemDO.getPrice()));
        itemModel.setStock(itemStockDO.getStock());
        return itemModel;
    }
}
