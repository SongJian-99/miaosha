package com.example.miaosha.service.model;

import java.math.BigDecimal;

//用户下单的交易模型
public class OrderModel {

    private String id;

    //购买的用户id
    private Integer userId;

    //购买的商品id
    private Integer itemId;

    //购买商品的单价，如果promoId不为空，则是秒杀的价格
    private BigDecimal itemPrice;

    //购买的数量
    private Integer amount;

    //购买金额（总金额），如果promoId不为空，则是秒杀的价格
    private BigDecimal orderPrice;

    //秒杀活动id,若为空则表示以秒杀商品下单
    private Integer promoId;


    public String getId() { 
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BigDecimal getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(BigDecimal orderPrice) {
        this.orderPrice = orderPrice;
    }

    public Integer getPromoId() {
        return promoId;
    }

    public void setPromoId(Integer promoId) {
        this.promoId = promoId;
    }
}
