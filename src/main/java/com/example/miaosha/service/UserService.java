package com.example.miaosha.service;
import com.example.miaosha.error.BusinessException;
import com.example.miaosha.service.model.UserModel;

public interface UserService {
    //通过用户id获取用户对象的方法
    UserModel findUserById(Integer id);

    /**
     * 用户注册功能
     * @param userModel
     * @throws BusinessException
     */
    void register(UserModel userModel) throws BusinessException;

    /**
     * 用户登录功能
     * @param telephone 用户手机号
     * @param encryptPassword 用户加密后的密码
     * @return
     */
    UserModel validateLogin(String telephone,String encryptPassword) throws BusinessException;

    /**
     * 通过缓存获取用户对象
     * @param id
     * @return
     */
    UserModel findUserByIdInCache(Integer id);
}
