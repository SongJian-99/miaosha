package com.example.miaosha.service;

//封装本地缓存操作类
public interface CacheService {
    //向缓存中存数据的方法
    void setCommonCache(String key,Object value);

    //从缓存中取数据的方法
    Object getFromCommonCache(String key);
}
