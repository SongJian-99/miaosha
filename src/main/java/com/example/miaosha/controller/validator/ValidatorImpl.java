package com.example.miaosha.controller.validator;

import org.apache.ibatis.scripting.xmltags.ForEachSqlNode;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

@Component
//InitializingBean接口为bean提供了初始化方法的方式，它只包括afterPropertiesSet方法
//凡是继承该接口的类，在初始化bean的时候都会执行该方法。
public class ValidatorImpl implements InitializingBean {

    private Validator validator;

    //实现校验方法并返回校验结果
    public ValidationResult validate(Object bean){
        ValidationResult result = new ValidationResult();
        //如果bean里面定义的参数规则有错误，就放到set集合中
        Set<ConstraintViolation<Object>> constraintViolationSet = validator.validate(bean);
        if(constraintViolationSet.size() > 0){
            //说明有错误
            result.setHasErrors(true);
            for (ConstraintViolation<Object> objectConstraintViolation : constraintViolationSet) {
                //获得错误信息
                String errMsg = objectConstraintViolation.getMessage();
                //获取错误的字段
                String propertyName = objectConstraintViolation.getPropertyPath().toString();
                result.getErrorMsgMap().put(propertyName,errMsg);
            }
        }
        return result;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        //将hibernate validator通过工厂的初始化方式使其实例化
        this.validator = Validation.buildDefaultValidatorFactory().getValidator();
    }
}
