package com.example.miaosha.controller;

import com.alibaba.druid.util.StringUtils;
import com.example.miaosha.controller.viewobject.UserVO;
import com.example.miaosha.error.BusinessException;
import com.example.miaosha.error.EmBusinessError;
import com.example.miaosha.response.CommonReturnType;
import com.example.miaosha.service.UserService;
import com.example.miaosha.service.model.UserModel;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import sun.misc.BASE64Encoder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;


@Controller
@RequestMapping("/user")
@CrossOrigin(allowCredentials = "true",originPatterns = "*")
public class UserController extends BaseController{

    @Resource
    private UserService userService;

    @Resource
    private RedisTemplate redisTemplate;

    //用户注册接口
    @RequestMapping(value = "/register",method = {RequestMethod.POST},consumes = {CONTENT_TYPE_FORMED})
    @ResponseBody
    public CommonReturnType register(@RequestParam("telephone")String telephone,
                                   @RequestParam("name")String name,
                                   @RequestParam("age")Integer age,
                                   @RequestParam("gender")Integer gender,
                                   @RequestParam("otpCode")String otpCode,
                                   @RequestParam("password")String password,
                                   HttpServletRequest request) throws BusinessException, UnsupportedEncodingException, NoSuchAlgorithmException {
        //判断用户的otp验证码
        String inSessionOtpCode = (String)request.getSession().getAttribute(telephone);
        if(!StringUtils.equals(inSessionOtpCode,otpCode)){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,"短信验证码不符合");
        }
        //用户注册验证
        UserModel userModel = new UserModel();
        userModel.setName(name);
        userModel.setGender(gender.byteValue());
        userModel.setAge(age);
        userModel.setTelephone(telephone);
        userModel.setRegisterMode("byphone");
        userModel.setEncryptPassword(this.EncodeByMd5(password));
        userService.register(userModel);
        return CommonReturnType.create(null);
    }


    @RequestMapping(value = "/login",method = {RequestMethod.POST},consumes = {CONTENT_TYPE_FORMED})
    @ResponseBody
    public CommonReturnType login(@RequestParam("telephone")String telephone,
                                  @RequestParam("password")String password,
                                  HttpServletRequest request) throws BusinessException, UnsupportedEncodingException, NoSuchAlgorithmException {
        //参数校验
        if(StringUtils.isEmpty(telephone) || StringUtils.isEmpty(password)){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        //用户信息校验
        UserModel userModel = userService.validateLogin(telephone, this.EncodeByMd5(password));

        //用户登录验证成功后将对应的登录信息和登录凭证一起存入redis中
        //使用UUID生成用户的token
        String uuidToken = UUID.randomUUID().toString();
        uuidToken = uuidToken.replace("-","");
        //建立token和用户登录态之间的联系
        redisTemplate.opsForValue().set(uuidToken,userModel);
        //设置过期时间为1小时
        redisTemplate.expire(uuidToken,1, TimeUnit.HOURS);
//        request.getSession().setAttribute("IS_LOGIN",true);
//        request.getSession().setAttribute("LOGIN_USER",userModel);
        //下发token
        return CommonReturnType.create(uuidToken);
    }

    //密码加密
    public String EncodeByMd5(String str) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        //确定计算方法
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        BASE64Encoder base64Encoder = new BASE64Encoder();
        //加密字符串
        String newStr = base64Encoder.encode(md5.digest(str.getBytes("utf-8")));
        return newStr;
    }
    //用户获取otp短信接口
    @RequestMapping(value = "/getOtp",method = {RequestMethod.POST},consumes = {CONTENT_TYPE_FORMED})
    @ResponseBody
    public CommonReturnType getOtp(@RequestParam("telephone")String telephone,HttpServletRequest request){
        //生成otp验证码
        Random random = new Random();
        int randomInt = random.nextInt(99999);   //[0,99999)
        randomInt += 10000;     //[10000,109999)
        String optCode = String.valueOf(randomInt);

        //将opt验证码和对应用户的手机号关联
        request.getSession().setAttribute(telephone,optCode);

        //将otp验证码通过短信通道发送给用户，省略     
        System.out.println("telephone=" + telephone + "&optCode=" + optCode);

        return CommonReturnType.create(null);
    }

    //根据id查询用户信息
    @RequestMapping("/get")
    @ResponseBody
    public CommonReturnType getUser(@RequestParam(name = "id")Integer id){
        UserModel usermodel = userService.findUserById(id);
        UserVO userVo = convertFromModel(usermodel);
        return CommonReturnType.create(userVo);
    }


    private UserVO convertFromModel(UserModel userModel){
        if(userModel == null){
            return null;
        }
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(userModel,userVO);
        return userVO;
    }
}
