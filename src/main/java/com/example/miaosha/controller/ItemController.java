package com.example.miaosha.controller;

import com.example.miaosha.controller.viewobject.ItemVO;
import com.example.miaosha.error.BusinessException;
import com.example.miaosha.response.CommonReturnType;
import com.example.miaosha.service.CacheService;
import com.example.miaosha.service.ItemService;
import com.example.miaosha.service.PromoService;
import com.example.miaosha.service.impl.CacheServiceImpl;
import com.example.miaosha.service.impl.ItemServiceImpl;
import com.example.miaosha.service.impl.PromoServiceImpl;
import com.example.miaosha.service.model.ItemModel;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/item")
@CrossOrigin(allowCredentials = "true",allowedHeaders = "*",originPatterns = "*")
public class ItemController extends BaseController{

    @Resource
    private ItemService itemService;

    @Resource
    private PromoService promoService;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private CacheService cacheService;

    //创建订单
    @RequestMapping(value = "/create",method = {RequestMethod.POST},consumes = {CONTENT_TYPE_FORMED})
    @ResponseBody
    public CommonReturnType createItem(@RequestParam("title")String title,
                                   @RequestParam("price")BigDecimal price,
                                   @RequestParam("stock")Integer stock,
                                   @RequestParam("description")String description,
                                   @RequestParam("imgUrl")String imgUrl) throws BusinessException {
        ItemModel itemModel = new ItemModel();
        itemModel.setTitle(title);
        itemModel.setPrice(price);
        itemModel.setStock(stock);
        itemModel.setDescription(description);
        itemModel.setImgUrl(imgUrl);
        ItemModel itemModelForReturn = itemService.createItem(itemModel);
        ItemVO itemVO = convertFromModel(itemModelForReturn);
        return CommonReturnType.create(itemVO);
    }

    @RequestMapping(value = "/publishPromo",method = {RequestMethod.GET})
    @ResponseBody
    public CommonReturnType publishPromo(@RequestParam("id")Integer id){
        promoService.publishPromo(id);
        return CommonReturnType.create(null);
    }


    //商品详情页浏览
    @RequestMapping(value = "/get",method = {RequestMethod.GET})
    @ResponseBody
    public CommonReturnType getItem(@RequestParam("id")Integer id){
        //多级缓存
        //先从本地缓存中查找数据
        ItemModel itemModel = (ItemModel) cacheService.getFromCommonCache("item_" + id);
        if(itemModel == null){
            //未命中本地缓存，再从redis中查询数据
            itemModel = (ItemModel)redisTemplate.opsForValue().get("item_" + id);
            //未命中redis，执行下游的service，查询数据库
            if(itemModel == null){
                itemModel = itemService.getItemById(id);
                //将订单信息存入redis中，并设置过期时间为10分钟
                redisTemplate.opsForValue().set("item_" + id,itemModel);
                redisTemplate.expire("item_" + id,10, TimeUnit.MINUTES);
            }
            //将数据存入本地缓存中
            cacheService.setCommonCache("item_" + id,itemModel);
        }
        ItemVO itemVO = convertFromModel(itemModel);
        return CommonReturnType.create(itemVO);
    }

    //商品列表页面浏览
    @RequestMapping(value = "/list",method = {RequestMethod.GET})
    @ResponseBody
    public CommonReturnType listItem(){
        List<ItemModel> itemModelList = itemService.listItem();
        List<ItemVO> itemVoList = new ArrayList<>();
        for (ItemModel itemModel : itemModelList) {
            itemVoList.add(convertFromModel(itemModel));
        }
        return CommonReturnType.create(itemModelList);
    }

    private ItemVO convertFromModel(ItemModel itemModel){
        if(itemModel == null){
            return null;
        }
        ItemVO itemVO = new ItemVO();
        BeanUtils.copyProperties(itemModel,itemVO);
        if(itemModel.getPromoModel() != null){
            //有正在进行或即将进行的秒杀活动
            itemVO.setPromoStatus(itemModel.getPromoModel().getStatus()); //秒杀商品的状态
            itemVO.setPromoId(itemModel.getPromoModel().getId());   //秒杀商品的id
//            itemVO.setStartTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(itemModel.getPromoModel().getStartTime().toDate()));
            itemVO.setStartTime(itemModel.getPromoModel().getStartTime().toString(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")));   //秒杀活动的开始时间
            itemVO.setPromoPrice(itemModel.getPromoModel().getPromoItemPrice());    //秒杀商品的价格
        }else{
            itemVO.setPromoStatus(0);
        }
        return itemVO;
    }
}
