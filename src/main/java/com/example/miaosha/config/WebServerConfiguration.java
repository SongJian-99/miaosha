package com.example.miaosha.config;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.stereotype.Component;

/**
 * 定制化内嵌Tomcat开发。
 *
 * Tomcat keepAlive优化。保护我们的系统可以不受客户端连接的拖累，合理利用服务端的资源
 *
 */

//当Spring容器内没有TomcatEmbeddedServletContainerFactory这个bean时，会把此bean加载进Spring容器
@Component
public class WebServerConfiguration implements WebServerFactoryCustomizer<ConfigurableWebServerFactory> {

    @Override
    public void customize(ConfigurableWebServerFactory factory) {
        //使用对应工厂类提供给我们的接口定制化我们的tomcat connector
        ((TomcatServletWebServerFactory)factory).addConnectorCustomizers(new TomcatConnectorCustomizer() {
            @Override
            public void customize(Connector connector) {
                Http11NioProtocol protocol =(Http11NioProtocol) connector.getProtocolHandler();
                //定制化KeepAliveTimeout,设置30秒内没有请求则服务端自动断开keepAlive连接
                protocol.setKeepAliveTimeout(30000); 
                //当客户端发送超过10000个请求，则自动断开keepAlive链接
                protocol.setMaxKeepAliveRequests(10000);
            }
        });
    }
}
